#!/bin/bash

# Setup xonstat for dockerization
git clone https://gitlab.com/xonotic/xonstat.git web/xonstat
sed -i 's/localhost:5432/db:5432/g' web/xonstat/development.ini
cp web/wait-for-postgres.sh web/xonstat/
chmod +x web/xonstat/wait-for-postgres.sh

# Setup xonstatdb for dockerization
# git clone https://gitlab.com/xonotic/xonstatdb.git db/xonstatdb