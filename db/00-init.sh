#!/bin/bash
set -e

cd /docker-entrypoint-initdb.d/

psql -v ON_ERROR_STOP=1 -U postgres <<-EOSQL
    CREATE USER xonstat WITH PASSWORD 'xonstat';
    CREATE DATABASE xonstatdb
      WITH ENCODING='UTF8'
        OWNER=xonstat
        CONNECTION LIMIT=-1;
    GRANT ALL PRIVILEGES ON DATABASE xonstatdb TO xonstat;
EOSQL

psql -v ON_ERROR_STOP=1 -U xonstat xonstatdb <<-EOSQL
    CREATE SCHEMA xonstat
        AUTHORIZATION xonstat;
    \i build/build_full.sql
EOSQL
